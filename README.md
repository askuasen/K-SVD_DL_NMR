# K-SVD_DL_NMR

## 介绍
This is a repository about paper "A Machine Learning Protocol for Low-field NMR Data Processing" denoise stage's algorithm, it's a python version.

## 软件依赖环境
Python>=3.7  
numpy>=1.17.2  
Scikit-learn>=0.24.2  
matplotlib>=3.2.0  
pandas==1.2.4  
scipy==1.4.1  


## 安装教程

1.  Check out your python enviroments, if not meet, please use 'pip install [package_name]' to make it available.


## 使用说明
The main program is ksvd_dllearning_2d.py  
The Algorithm of K-SVD and OMP are in custom_ksvd_dict_learning.py

1.  Modified the variable 'orgin_data_path' in ksvd_dllearning_2d.py with no noise data file.  
2.  Modified the variable 'orgin_data_distorted_path' in ksvd_dllearning_2d.py with noise data file.
3.  Run 'python ksvd_dllearning_2d.py'  


## Results
The Reconstruction of Low NMR 
![avatar](figure/reconstruction.png)  
The Denoise NMR signal 2D by using K-SVD and OMP. 
![avatar](figure/recon2D.png)  
![avatar](figure/denoise.png) 
The final results 
![avatar](figure/final.png) 


## 参与贡献

1.  罗嗣慧待完善



#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
